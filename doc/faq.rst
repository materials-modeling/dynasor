.. index:: FAQ

Frequently asked questions
***************************

Here are answers to some frequently asked questions.
Feel free to also read through previously asked questions by users on `matsci.org <https://matsci.org/dynasor>`_ and in the `gitlab issue tracker <https://gitlab.com/materials-modeling/dynasor/-/issues?sort=updated_desc&state=all&label_name[]=User>`_.



Units of q-points
-----------------
The :math:`\boldsymbol{q}`-points in dynasor includes the :math:`2\pi` factor, meaning the units of :math:`\boldsymbol{q}`-points are rad/Å.
This is analogous to how angular frequencies have units of rad/fs.
See :ref:`documentation of units <workflow>` for more details
