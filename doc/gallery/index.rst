.. _gallery:

.. index:: gallery

Gallery
********

Here, we provide a simple collection of publications containing results that have been obtained using :program:`dynasor`.

Structure factors in BaZrO\ :sub:`3`
------------------------------------
Simulated diffuse scattering in BaZrO\ :sub:`3`, from `Fransson et al. Chemisty of Materials 2023 <https://doi.org/10.1021/acs.chemmater.3c02548>`_.

.. figure:: figs/BaZrO_Sq.png
    :align: center

|

Phonons in moiré structures
---------------------------
Phonon mode projections for disordered moiré structures, from `Eriksson et al. ACS Nano 2023 <https://doi.org/10.1021/acsnano.3c09717>`_.

.. figure:: figs/moire_dispersions.png
    :align: center

|

Static structure factors and XRD for BaZrS\ :sub:`3`
----------------------------------------------------
Simulated x-ray diffraction patterns in BaZrS\ :sub:`3`, from `Kayastha et al. <https://doi.org/10.1021/acs.jpclett.4c03517>`_.

.. figure:: figs/BaZrS3_Sq.png
    :align: center

|

Overdamped phonon modes in CsPbBr\ :sub:`3`
-------------------------------------------
Overdamped phonon modes in CsPbBr\ :sub:`3` obtained via mode projections, from `Fransson et al. Communication Physics 2023 <https://doi.org/10.1038/s42005-023-01297-8>`_.

.. figure:: figs/CsPbBr_overdamped_modes.png
    :align: center

|

Current correlations in molten NaCl
-----------------------------------
Current correlations in molten NaCl, from `Fransson et al. Advanced Theory and Simulations 2021 <https://doi.org/10.1002/adts.202000240>`_.

.. figure:: figs/NaCl_liquid.png
    :align: center
