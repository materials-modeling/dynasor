.. _credits:
.. index:: Credits

.. |br| raw:: html

  <br/>


Credits
*******

:program:`dynasor` has been developed at the Condensed Matter and Materials Theory division at the Department of Physics at Chalmers University of Technology. 
The development of this package has been supported by the Knut och Alice Wallenbergs Foundation, the Swedish Research Council, the Swedish Foundation for Strategic Research, and the Swedish National Infrastructure for Computing.

Below is the list of main contributors 

* Mattias Slabanja
* Erik Fransson
* Esmée Berger
* Fredrik Eriksson
* Paul Erhart

When using :program:`dynasor` in your research please cite the following paper:

* *Dynasor – A tool for extracting dynamical structure factors and current correlation functions from molecular dynamics simulations* |br|
  Erik Fransson, Mattias Slabanja, Paul Erhart, and Göran Wahnström |br|
  Advanced Theory and Simulations **4**, 2000240 (2021); DOI:`10.1002/adts.202000240 <https://doi.org/10.1002/adts.202000240>`_
