.. _theory:



.. index:: Theory
.. index:: Background

Theoretical background
**********************

Below follows a brief overview of the theory underpinning the correlation functions supported by :program:`dynasor` as well as their analysis.
For a more detailed description and derivations of the correlations functions see for example :cite:`BoonYip`.

.. index:: Correlation functions; Position


Dynamic structure factor
------------------------

The particle density is denoted :math:`n(\boldsymbol{r},t)` and defined as

.. math::

   n(\boldsymbol{r},t) = \sum _i ^N \delta (\boldsymbol{r} - \boldsymbol{r}_i(t)),

where :math:`N` is the number of particles and :math:`\boldsymbol{r}_i(t)` is the position of particle :math:`i` at time :math:`t`.
The intermediate scattering function :math:`F(\boldsymbol{q},t)` is defined as the Fourier transform of the auto-correlation of the particle density

.. math::

   F(\boldsymbol{q},t)=\frac{1}{N}\left<n(\boldsymbol{q},t)n(-\boldsymbol{q},0)\right>
   \,= \, \frac{1}{N}\sum _i ^N \sum _j ^N \left<
   \mathrm{exp}
   \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right]
   \right>,

where :math:`\boldsymbol{q}` is a wave vector.
The brackets denote an ensemble average, which in the case of :program:`dynasor` is replaced by a time average.
(The system under study should be ergodic in order for the time average to be a suitable substitute for the ensemble average.)
Here, :math:`F(\boldsymbol{q},t)` is commonly referred to as the coherent part.

.. index:: Self part
	   
The incoherent intermediate scattering function, i.e., :math:`i=j`, is defined as

.. math::

   F_\mathrm{incoh}(\boldsymbol{q},t)=\frac{1}{N} \sum _i ^N \left <
   \mathrm{exp}\left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_i(0))\right]
   \right >

.. index:: Incoherent scattering function
.. index:: Scattering function; Incoherent

and describes single particle motion, i.e., diffusive motion.
:math:`F_\mathrm{incoh}(\boldsymbol{q},t)` is commonly referred to as the incoherent scattering function, but sometimes also referred to as the self-part of the intermediate scattering function.

The static structure factor is given by

.. math::

   S(\boldsymbol{q}) = F(\boldsymbol{q},0),

.. index:: Structure factor; dynamic
.. index:: Dynamic structure factor
		     
whereas the dynamic structure factor :math:`S(\boldsymbol{q},\omega)` is the Fourier transform of :math:`F(\boldsymbol{q},t)`

.. math::

   S(\boldsymbol{q},\omega) = \int _{-\infty} ^\infty
   F(\boldsymbol{q},t) \, e^{-iwt} \mathrm{d}t,

where :math:`\omega` is the angular frequency.
:math:`S(\boldsymbol{q},\omega)` exhibits peaks in the :math:`(\boldsymbol{q},\omega)` plane corresponding to the dispersion of lattice vibrations (phonons).
The widths of these peaks are related to the phonon lifetimes.
Computing :math:`S(\boldsymbol{q},\omega)` via molecular dynamics simulation has the advantage of *fully* including anharmonic effects and allowing one to study the temperature dependence of phonon dispersions.


.. index:: Correlation functions; Velocity
.. index:: Correlation functions; Current
.. index:: Velocity correlation functions
.. index:: Current correlation functions

Velocity correlation functions
------------------------------

It is often convenient to also consider current correlations based on the particle velocities.
The current density :math:`\boldsymbol{j}(\boldsymbol{r},t)` is given by

.. math::

   \boldsymbol{j}(\boldsymbol{r},t)
   &= \sum_i^N \boldsymbol{v}_i(t) \, \delta (\boldsymbol{r} - \boldsymbol{r}_i(t)) \\
   \boldsymbol{j}(\boldsymbol{q},t)
   &= \sum_i^N \boldsymbol{v}_i(t) \, \mathrm{e}^{\mathrm{i}\boldsymbol{q} \cdot \boldsymbol{r}_i(t)},

where :math:`\boldsymbol{v}_i(t)` is the velocity of particle :math:`i` at time :math:`t`.
This can be split into a longitudinal and transverse part as

.. math::

   \boldsymbol{j}_L(\boldsymbol{q},t)
   &= \sum_i ^N(\boldsymbol{v_i}(t) \cdot\hat{\boldsymbol{q}}) \, \hat{\boldsymbol{q}}
   \, \mathrm{e}^{\mathrm{i}\boldsymbol{q} \cdot \boldsymbol{r}_i(t)} \\
   \boldsymbol{j}_T(\boldsymbol{q},t)
   &= \sum_i ^N \left[\boldsymbol{v_i}(t) - (\boldsymbol{v_i}(t) \cdot \hat{\boldsymbol{q}}) \, \hat{\boldsymbol{q}}\right]
   \, \mathrm{e}^{\mathrm{i}\boldsymbol{q} \cdot \boldsymbol{r}_i(t)}.

Now the correlation functions can be computed as

.. math::

   C_L(\boldsymbol{q},t) &= \frac{1}{N}\left<\boldsymbol{j}_L(\boldsymbol{q},t)\cdot\boldsymbol{j}_L(-\boldsymbol{q},0)\right> \\
   C_T(\boldsymbol{q},t) &= \frac{1}{N}\left<\boldsymbol{j}_T(\boldsymbol{q},t)\cdot\boldsymbol{j}_T(-\boldsymbol{q},0)\right>.

The longitudinal current can be related to the particle density as

.. math::

   \frac{\partial }{\partial t}n(\boldsymbol{q}, t)
   &= \mathrm{i} \boldsymbol{q}\cdot \boldsymbol{j}(\boldsymbol{q}, t) \\
   \omega^2 S(\boldsymbol{q},\omega)
   &= q^2 C_L(\boldsymbol{q},\omega),

which means some features can be easier to resolve in one function than the other.
The current correlation functions can be thought of as spatially-dependent generalization of the velocity correlation function and are closely related to the phonon spectral energy density.


Multi-component systems
-----------------------

In multi-component systems one can introduce partial correlations functions, which enables separation of the contributions by groups of particles, e.g., by type or site symmetry.
For example, in the case of a binary system, one can define :math:`F_\mathrm{AA}(\boldsymbol{q},t)`, :math:`F_\mathrm{BB}(\boldsymbol{q},t)`, :math:`F_\mathrm{AB}(\boldsymbol{q},t)` and so on for all correlation functions introduced above.
In :program:`dynasor` we define the partial correlaion functions as

.. math::

   F_\mathrm{AA}(\boldsymbol{q},t) = \frac{1}{N}\sum_{i\in A}^{N_\mathrm{A}} \sum_{j\in A}^{N_\mathrm{A}} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>

where the sums runs over all A atoms.
Here, :math:`N_\mathrm{A}` refers to number of atoms of type A, :math:`N_\mathrm{B}` refers to number of atoms of type B, and :math:`N = N_\mathrm{A} + N_\mathrm{B}` refers to the total number of atoms in the system.
For the cross-term, :math:`F_\mathrm{AB}(\boldsymbol{q},t)`, we get

.. math::

   F_\mathrm{AB}(\boldsymbol{q},t) = & \frac{1}{N}\sum_{i\in A}^{N_\mathrm{A}} \sum_{j\in B}^{N_\mathrm{B}} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>
   +
   \frac{1}{N}\sum_{i\in B}^{N_\mathrm{B}} \sum_{j\in A}^{N_\mathrm{A}} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right> \\=& \frac{2}{N}\sum_{i\in A}^{N_\mathrm{A}} \sum_{j\in B}^{N_\mathrm{B}} \left< \mathrm{exp} \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>

Here, the factor two in the final expression is due to considering both the A-B and B-A terms.
These normalization choices means the total intermediate scattering function :math:`F(\boldsymbol{q},t)` (as defined above) is given by

.. math::
   
   F(\boldsymbol{q},t) = F_\mathrm{AA}(\boldsymbol{q},t) + F_\mathrm{AB}(\boldsymbol{q},t) + F_\mathrm{BB}(\boldsymbol{q},t)

In some cases, instead of analyzing the partial functions directly, it is interesting to consider linear combinations of them. For example weighting them with relevant atomic form factors, masses or charges etc.

Relation to scattering experiments
----------------------------------

The correlation functions can be convoluted with atomic weights, :math:`w_i`, i.e.,

.. math::

   F_\mathrm{coh}(\boldsymbol{q},t)=\frac{1}{N}\sum _i ^N \sum _j ^Nw_iw_j \left<
   \mathrm{exp}
   \left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right]
   \right>,
   
where the weights could for example be masses, charges, or even atomic form factors (scattering lengths) to allow for direct comparison to structure factors measured in various neutron and X-ray scattering experiments. For some probes, e.g., neutrons, the scattering lenghts are in general constant but differ for the coherent and incoherent correlation functions. X-ray form factors, on the other hand, are :math:`q`-dependent and thus take the form 

.. math::

   F_\mathrm{coh}(\boldsymbol{q},t)=\frac{1}{N}\sum _i ^N \sum _j ^Nf_i(q)f_j(q) \left< \mathrm{exp}\left[i\boldsymbol{q}\cdot(\boldsymbol{r}_i(t)-\boldsymbol{r}_j(0))\right] \right>.


Conveniently, since the weights only depend on the atom type, the weighting can be done as a post-processing step using partial correlation functions. For a binary (A-B) system, the total weighted correlation function is

.. math::

    F_\mathrm{coh}(\boldsymbol{q},t)=f^{AA}(q) F^{AA}_\mathrm{coh}(\boldsymbol{q},t) + f^{AB}(q) F^{AB}_\mathrm{coh} (\boldsymbol{q},t)  + f^{BB}(q) F^{BB}_\mathrm{coh}(\boldsymbol{q},t).


Furthermore, it is frequently desirable to determine the above mentioned quantites along specific paths between high-symmetry :math:`\boldsymbol{q}`-points when working with solids.
In isotropic samples, such as for example liquids, it is on the other hand usually preferable to compute these functions with respect to :math:`q=|\boldsymbol{q}|`, i.e., a spherical average over wave vectors.


.. index:: Spectral energy density; SED

Spectral energy density method
------------------------------

The spectral energy density (:term:`SED`) is closely related to the phonon dispersion and thus also to the current correlations above.
It is a measure of how the kinetic energy is partitioned over different wavelengths and frequencies in the system.
The theoretical background for :term:`SED` is quite simple and can be derived in several ways.
Formally the kinetic action :math:`W` of the system can be written as

.. math::
        
        W = \int dt \sum_n \sum_i \frac{1}{2} m_i v_i(n, t)^2.

Here, :math:`n` is the cell index and :math:`i` is the basis index, which together uniquely identify each atom in an infinite crystal.
By using Parseval's theorem for both the time integration and space summation the above is transformed to inverse spatial :math:`q`-space and inverse temporal :math:`\omega`-space

.. math::

        W = \int d\omega \sum_q \sum_i \frac{1}{2} m_i v_i(q, \omega)^2.


This is motivated by the Wiener–Khinchin theorem and the :term:`SED` is simply defined as the quantity

.. math::

        \text{SED}(q, \omega) = \sum_i \frac{1}{2} m_i v_i(q, \omega)^2.

For a more in-depth discussion see, e.g., :cite:`Thomas2010`.


.. index:: Harmonic oscillator; Damped; DHO
.. index:: Power spectral density; PSD
.. index:: Damped harmonic Oscillator
.. index:: Autocorrelation function; ACF

Damped harmonic oscillator model
--------------------------------


The correlation functions can be fitted to analytical expressions for a damped harmonic oscillator (:term:`DHO`) in order to extract phonon frequencies and lifetimes.
The governing differential equation for the displacement (or coordinate) of a :term:`DHO`, :math:`x(t)`, is

.. math:: 
   \ddot{x}(t) + \Gamma \dot{x}(t) + \omega_0^2 x(t) = f(t).

Here, :math:`\omega_0` is the natural angular frequency and :math:`\Gamma` is the damping constant in units of reciprocal time.
Note that we work in units where the mass :math:`m=1`.
Below follow the analytical solutions for the equations of motion of a :term:`DHO` under the assumption that the stochastic force, :math:`f(t)`, is white-noise.

The :term:`DHO` can be either underdamped or overdamped.
In the underdamped case :math:`\omega_0\tau > 1`, where :math:`\tau = \frac{2}{\Gamma}` is commonly referred to as the lifetime of the :term:`DHO`.
The position autocorrelation function (:term:`ACF`) in the underdamped case is given by

.. math::

   F(t) = A \mathrm{e}^{- t/\tau} \left ( \cos{ \omega_e t} + \frac{1}{\tau\omega_e}\sin{ \omega_e t}\right ),

where the angular eigenfrequency of the :term:`DHO` is :math:`\omega_e = \sqrt{\omega_0^2 - 1/\tau^2}` and :math:`A` is a constant amplitude :math:`A=F(t=0)`.
The functional form is only valid for time lag :math:`t\geq 0` and should be interpreted as even, so that :math:`t\rightarrow -t` for negative times.
Alternatively replace :math:`t` with :math:`|t|` in the expressions for the :term:`ACFs <ACF>`.

In the overdamped case, :math:`\omega_0 \tau < 1`, the autocorrelation function is given by

.. math::

   F(t) = \frac{A}{\tau_\text{L} - \tau_\text{S}} \left(  \tau_\text{L} \text{e}^{-t / \tau_\text{L}}  - \tau_\text{S} \text{e}^{-t / \tau_\text{S}} \right),

where :math:`\tau_\text{S}` and :math:`\tau_\text{L}` correspond to the short and long relaxation times, respectively, defined as

.. math::

   \tau_\text{S,L} = \frac{\tau}{1 \pm \sqrt{1-(\omega_0\tau)^2}}.


The corresponding underdamped and overdamped velocity :term:`ACFs <ACF>` are related to the position :term:`ACFs <ACF>` via :math:`F_v(t) = -\ddot{F}(t)`:

.. math::
   F_v(t) &= A \omega_0^2 \mathrm{e}^{- t/\tau} \left ( \cos \omega_e t - \frac{1}{\tau\omega_e}\sin \omega_e t\right) \quad (\text{underdamped}) \\
   F_v(t) &= \frac{A}{\tau_\text{L}-\tau_\text{S}} \left(  \frac{1}{\tau_\text{S}} \text{e}^{-t / \tau_\text{S}} - \frac{1}{\tau_\text{L}} \text{e}^{-t / \tau_\text{L}}  \right ) \quad (\text{overdamped})



The analysis can also be carried out in the frequency domain by computing the power spectral density (:term:`PSD`) functions, which are Fourier transforms of the corresponding :term:`ACFs<ACF>`.
The position :term:`PSD` of a :term:`DHO` is given by

.. math::

   S(\omega) = A\frac{2\Gamma \omega_0^2}{(\omega^2 - \omega_0^2)^2 + (\Gamma\omega)^2},

which exhibits a peak at :math:`\omega_\text{max} = \sqrt{\omega_0^2 - \Gamma^2/2}`.

The :term:`PSD` of the velocity, :math:`S_v(\omega) = \omega^2 S(\omega)`, is

.. math::

   S_v(\omega) = A\frac{2\Gamma \omega_0^2 \omega^2}{(\omega^2 - \omega_0^2)^2 + (\Gamma\omega)^2}.

The velocity :term:`PSD` has a peak at :math:`\omega_\text{max} = \omega_0`.
The amplitude :math:`A` can be related to the variance (strength) :math:`\sigma^2=\langle f^2 \rangle` of the noise by :math:`2\Gamma\omega_0^2 A=\sigma^2`.
Taking the strength to be equal to :math:`\sigma^2=2\Gamma k_B T` the mean squared displacement becomes :math:`\langle x^2 \rangle=\frac{k_B T}{\omega_0^2}` and mean squared velocity is given by :math:`\langle v^2 \rangle=k_B T`.

.. figure:: _static/dho.svg
    :align: center
    :scale: 50%

    The position and velocity :term:`ACF` and :term:`PSD` solutions for the :term:`DHO` with natural angular frequency :math:`\omega_0 = 1` for a few different values of damping constant :math:`\Gamma`.
    The red line corresponds to the critical damping.
    For the position :term:`ACF` a critically damped oscillator goes to zero without any oscillation.
    When the damping is larger than :math:`\sqrt{2}` the peak for the corresponding :term:`PSD` fixates at zero.


.. index:: Phonon life time
.. index:: Life times; phonon

