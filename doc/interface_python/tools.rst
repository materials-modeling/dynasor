Tools
*****

.. automodule:: dynasor.tools.acfs
   :members:
   :inherited-members:

.. automodule:: dynasor.tools.damped_harmonic_oscillator
   :members:
   :inherited-members:
