.. _correlation_functions:

Correlation functions
*********************

The interface to :program:`dynasor` when computing structure factors consists of two functions, which are used for dynamic and static correlations, respectively.

* :func:`compute_dynamic_structure_factors <dynasor.compute_dynamic_structure_factors>`
* :func:`compute_static_structure_factors <dynasor.compute_static_structure_factors>`

:program:`dynasor` also provides functionality for computing the spectral energy density (SED) via the function :func:`compute_spectral_energy_density <dynasor.compute_spectral_energy_density>` :cite:`Thomas2010`.

.. autofunction:: dynasor.compute_dynamic_structure_factors

.. autofunction:: dynasor.compute_static_structure_factors

.. autofunction:: dynasor.compute_spectral_energy_density
