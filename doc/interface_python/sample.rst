Sample
******

The static and dynamic structure factors as well as current correlation functions generated by the main :ref:`analysis functions <correlation_functions>` are stored in :class:`Sample` objects.

Information about the **dynamic correlations** generated by the :func:`compute_dynamic_structure_factors <dynasor.compute_dynamic_structure_factors>` function is provided in the form of a :class:`DynamicSample <dynasor.sample.DynamicSample>` object.
The latter allows one to access, for example, the intermediate scattering functions via the following attributes:

.. list-table::
   :widths: 15 70 15
   :header-rows: 1
   :class: tight-table

   * - Attribute
     - Explanation
     - Symbol
   * - :attr:`Fqt_coh_A_B`
     - partial coherent intermediate scattering function for atom types A and B
     - :math:`F(\boldsymbol{q},t)_\mathrm{AB}^\mathrm{coh}`
   * - :attr:`Fqt_coh`
     - total coherent intermediate scattering function
     - :math:`F(\boldsymbol{q},t)^\mathrm{coh}`
   * - :attr:`Fqt_incoh_A`
     - partial incoherent intermediate scattering function for atom type A
     - :math:`F(\boldsymbol{q},t)_\mathrm{A}^\mathrm{incoh}`
   * - :attr:`Fqt_incoh`
     - total incoherent intermediate scattering function
     - :math:`F(\boldsymbol{q},t)^\mathrm{incoh}`
   * - :attr:`Fqt`
     - total intermediate scattering function
     - :math:`F(\boldsymbol{q},t)^\mathrm{coh} + F(\boldsymbol{q},t)^\mathrm{incoh}`

Similarly, one can access

* dynamic structure factors :math:`S(\boldsymbol{q}, \omega)` via attributes starting with :attr:`Sqw`
* the longitudinal and transverse current correlation functions in the time domain, :math:`C_L(\boldsymbol{q}, t)` and :math:`C_T(\boldsymbol{q}, t)`, via attributes starting with :attr:`Clqt` and :attr:`Ctqt`, respectively, and
* the longitudinal and transverse current correlation functions in the frequency domain, :math:`C_L(\boldsymbol{q}, \omega)` and :math:`C_T(\boldsymbol{q}, \omega)`, via attributes starting with :attr:`Clqw` and :attr:`Ctqw`, respectively.

Information about **static correlations** can be obtained via the :func:`compute_static_structure_factors <dynasor.compute_static_structure_factors>` function, which returns a :class:`StaticSample <dynasor.sample.StaticSample>` object.
The latter allows one to access the static structure factor via the attribute :attr:`Sq`.

.. autofunction:: dynasor.read_sample_from_npz

.. autoclass:: dynasor.sample.DynamicSample
   :members:
   :inherited-members:

.. autoclass:: dynasor.sample.StaticSample
   :members:
   :inherited-members:
