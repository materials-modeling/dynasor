.. index:: Tutorials; time_convergence

Time convergence
****************
Here, a short demonstration on how time-correlation typically converges with total simulation time is provided for FCC Al at :math:`T =` 300 K. To get a converged correlation function, e.g. in order to extract a phonon lifetime, one require on the order 100-1000 times longer simulation than the lifetime itself. Rather than running a single long MD simulation one can also many shorter simulations and average their correlation functions.

In the figure below, the decay time of the correlation function is about 1-2 ps. The :math:`C_L(q, t)` is pretty well-converged after 200 ps and very well converged after 2000 ps.


.. figure:: figs/Al_solid_acf_convergence.svg
    :scale: 100 %
    :align: center

    Current correlation :math:`C_L(q, t)` for simulations of varying lengths.

