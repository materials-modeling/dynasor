.. _tutorials:

.. index:: tutorials

Tutorials
*********

Below (and in the ``examples`` folder on `gitlab <https://gitlab.com/materials-modeling/dynasor>`_) you can find serveral tutorials and examples that demonstate how to use :program:`dynasor`.

In setting up these examples, suitable parameters were chosen for the MD simulations that provide physically sound (albeit not necessarily highly converged) results.
In practice, it is strongly recommended to carefully test the effect of e.g., system size, sampling time, spacing of snapshots, possibly interatomic potential/force field, and :program:`dynasor` averaging parameters.

Getting started
===============

To begin with, there are two tutorials about dynamics in aluminum. Here you will learn the basics, like setting up a trajectory, setting up the q-points to sample (along a path in the solid case and spherically averaged in the liquid case),  running a :program:`dynasor` calculation, understanding the contents of a :program:`dynasor`-sample, and some basic visualization of the results. 

.. toctree::
   :glob:
   :maxdepth: 1

   aluminum_liquid_dynamic
   aluminum_solid_dynamic

Advanced applications
=====================

These tutorials cover some more advanced topics, such as splitting structure factors by species, some convergence considerations, and spectral energy density calculations.

In this tutorial you will learn how to handle multi-component systems by computing full and partial dynamic structure factors and current correlation for ordered and disordered NiAl. 

.. toctree::
   :glob:
   :maxdepth: 1

   NiAl-ordered-and-disordered

In this tutorial you will learn how to compute the partial static structure factors for the different components of a halide perovskite and study the convergence of the results with system size and trajectory length. 

.. toctree::
   :glob:
   :maxdepth: 1

   static_structure_factor

Here, you will learn how to compute the spectral energy density from a molecular dynamics simulation of a crystal with :program:`dynasor`, which captures the full anharmonicity, and compare this to a phonopy dispersion, which is based on 0 K lattice dynamics.    

.. toctree::
   :glob:
   :maxdepth: 1

   sed

Post-processing
===============

In these tutorials you will learn about ways to post-process the :program:`dynasor` results, for example weighting of structure factors to compare with different scattering experiments, or extracting phonon frequencies and lifetimes using the damped harmonic oscillator model.

.. toctree::
   :glob:
   :maxdepth: 1

   NiAl-neutron-and-Xray-weighting
   dho_peak_fitting

Other topics
============

This tutorial provides some general insights into auto-correlation functions, Fourier transforms, and numerical tools for decreasing noise. 

.. toctree::
   :glob:
   :maxdepth: 1

   acfs_and_ffts

This example serves to demonstrate the typical convergence of a correlation function with total simulation time. 

.. toctree::
   :glob:
   :maxdepth: 1

   time_convergence
